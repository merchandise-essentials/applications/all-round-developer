# All-round Developer Application

Use this project to start applying for the job All-round Developer with Merchandise Essentials. All information is included in the README-file.

## Installation steps

This project was installed through the installation command `composer create-project --prefer-dist laravel\laravel all-round-developer`.

## Before you start

1.  Create a new branch using your snake-cased full name.
2.  Configure a simple database using SQLite - make sure it's included in your future commits.
(e.g. https://www.codementor.io/@goodnesskay/developing-locally-using-sqlite-with-laravel-on-ubuntu-8s8358503)
3.  Develop a basic website structure with a header, body and footer using the framework Laravel.
The website should be fully responsive and be mobile friendly. Add some text and images representing your resume on the homepage.
4.  Provide the possibility to upload a CSV-file with the following columns: technology, description and years of experience.
Use your personal values. Using SQLite, save these values to the database. Feel free to validate the imported information.
It is possible to upload files more than once to complete the list. Make sure the database is included in your repository.
5.  Create a second page which shows the list of those uploaded records. Make it possible to adjust a record.
Validate entered data through javascript before server-side processing.
6.  You’re free to finish the complete project with your own style.
7.  Submit your branch and create a [Merge Request](https://gitlab.com/merchandise-essentials/applications/all-round-developer/-/commits/master).

## Guidelines

Following guidelines is key for teamwork. You're free to use external sites and guides, although it's appreciated if you include a list of the
specific guides or resources used in this project.

Do include

*  best practices applied (e.g. https://www.laravelbestpractices.com/, ...)
*  themes and layouts used (e.g. https://getbootstrap.com/docs/4.3/getting-started/introduction/, ...)
*  ...

to structure your sources.

No need to include sources like
* Google
* Github
* Stackoverflow

## Feedback and questions

Feel free to ask for feedback during implementation or after applying.

Email: bartel@merchandise-essentials.com
