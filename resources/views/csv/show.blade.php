@extends('layouts.app')

@section('content')
    <div class="card text-center w-75 mx-auto">
        <div class="card-header">
            Record Of {{ $data->created_at }}
        </div>
        <div class="card-body">
            <h5 class="card-title">Technology: <strong>{{ $data->technology }}</strong></h5>
            <h5 class="card-title">Description: <strong>{{ $data->description }}</strong></h5>
            <h5 class="card-title">Year of Experience: <strong>{{ $data->experience }}</strong></h5>
            <a class="btn btn-small btn-info" href="{{ URL::to('csv/' . $data->id . '/edit') }}">Edit Record</a>
        </div>
        <div class="card-footer text-muted">
            Record Of {{ $data->updated_at }}
        </div>
    </div>

@endsection
