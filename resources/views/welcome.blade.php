<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- awesome-fonts -->
        <script src="https://kit.fontawesome.com/b3478a18b6.js" crossorigin="anonymous"></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 6vw;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .font-20px {
                font-size: 20px;
            }
            .dev-img {
                width: 150px;
                height: auto;
            }
            .icon-100px {
                font-size: 7vw;
                margin-top: 1rem;
            }

        </style>
    </head>
    <body class="container-fluid">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md font-weight-bolder">
                    PHP DEVELOPER
                </div>

                <div class="font-20px w-75 mx-auto">
                    <p>I design and code beautifully simple things, and I love what I do.</p>
                    <img class="dev-img rounded-circle" src="{{ asset('img/awet.jpg') }}" alt="dev-image">
                </div>
            </div>
        </div>

        <div class="bg-dark text-center text-white p-3">
            <h3>Hi, I’m Matt. Nice to meet you.</h3>
            <p class="font-20px">Since beginning my journey as a freelance designer nearly 10 years ago, I've done remote work for agencies, consulted for startups, and collaborated with talented people to create digital products for both business and consumer use. I'm quietly confident, naturally curious, and perpetually working on improving my chops one design problem at a time.</p>
        </div>

        <div class="card-group mt-5 text-center">
            <div class="card">
                <i class="card-img-top fas fa-drafting-compass icon-100px"></i>
                <div class="card-body">
                    <h5 class="card-title">Front End</h5>
                    <p class="card-text">I value simple content structure, clean design patterns, and thoughtful interactions.</p>
                    <p class="card-text"><strong class="border-bottom">Technologies I use:</strong></p>
                    <p>HTML & HTML5</p>
                    <p>CSS & SASS</p>
                    <p>Javascript & TypeScript</p>
                </div>
            </div>
            <div class="card">
                <i class="fas fa-file-code card-img-top icon-100px"></i>
                <div class="card-body">
                    <h5 class="card-title">Back End</h5>
                    <p class="card-text">I like to code things from scratch, and enjoy bringing ideas to life in the browser.</p>
                    <p class="card-text"><strong class="border-bottom">Technologies I use:</strong></p>
                    <p>PHP & Symfony</p>
                    <p>PHP & Laravel</p>
                    <p>NodeJs & Angular</p>
                </div>
            </div>
            <div class="card">
                <i class="fas fa-database card-img-top icon-100px"></i>
                <div class="card-body">
                    <h5 class="card-title">Integration</h5>
                    <p class="card-text">I genuinely care about people, and love helping fellow designers work on their craft.</p>
                    <p class="card-text"><strong class="border-bottom">Technologies I use</strong></p>
                    <p>Docker & Compose</p>
                    <p>Git & GitHub</p>
                    <p>Mysql & Postgresql</p>
                </div>
            </div>
        </div>

        <div class="text-center mt-5">
            <h3>My Recent Work</h3>
            <p>Here are a few design projects I've worked on recently. Want to see more? Email me.</p>

            <div class="container">
                <div class="row">
                    <div class="col-md">
                        <a href="https://car-ride.herokuapp.com/">
                            <img class="w-100" src="{{ asset('img/car-ride.png') }}" alt="car-rent">
                        </a>
                    </div>
                    <div class="col-md">
                        <a href="https://electricity-service.herokuapp.com/">
                            <img class="w-100" src="{{ asset('img/electricity-service.png') }}" alt="electricity-service">
                        </a>
                    </div>
                    <div class="col-md">
                        <a href="https://pay-role-staff.herokuapp.com/">
                            <img class="w-100" src="{{ asset('img/payroll.png') }}" alt="payroll">
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <footer class="mt-5 text-center text-white" style="background-color: #f1f1f1;">
            <!-- Grid container -->
            <div class="container pt-4">
                <!-- Section: Social media -->
                <section class="mb-4">
                    <!-- Facebook -->
                    <a
                        class="btn btn-link btn-floating btn-lg text-dark m-1"
                        href="#!"
                        role="button"
                        data-mdb-ripple-color="dark"
                    ><i class="fab fa-facebook-f"></i
                        ></a>

                    <!-- Linkedin -->
                    <a
                        class="btn btn-link btn-floating btn-lg text-dark m-1"
                        href="#!"
                        role="button"
                        data-mdb-ripple-color="dark"
                    ><i class="fab fa-linkedin"></i
                        ></a>
                    <!-- Github -->
                    <a
                        class="btn btn-link btn-floating btn-lg text-dark m-1"
                        href="#!"
                        role="button"
                        data-mdb-ripple-color="dark"
                    ><i class="fab fa-github"></i
                        ></a>
                </section>
                <!-- Section: Social media -->
            </div>
            <!-- Grid container -->

            <!-- Copyright -->
            <div class="text-center text-dark p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                © 2021 Copyright:
                <a class="text-dark" href="/">Awet Tesfay</a>
            </div>
            <!-- Copyright -->
        </footer>
    </body>
</html>
