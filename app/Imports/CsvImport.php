<?php

namespace App\Imports;

use App\Csv;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CsvImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Csv([
            'technology' => $row['technology'],
            'description' => $row['description'],
            'experience' => $row['experience']
        ]);
    }
}
