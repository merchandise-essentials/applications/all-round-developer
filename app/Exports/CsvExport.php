<?php

namespace App\Exports;

use App\Csv;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class CsvExport implements FromCollection
{
    /**
    * @return Collection
    */
    public function collection(): Collection
    {
        return Csv::all();
    }
}
